import React, { Component } from 'react';
import { Link } from 'react-router-dom';
//import { Link } from 'react-router-dom';

class List extends Component {


    state = {
        loaded: false,
        data: []
    }

    componentDidMount(){
        fetch("https://jsonplaceholder.typicode.com/posts/")
            .then( res => res.json() )
            .then( res => {
                console.log('ress', res );
                this.setState({
                    loaded: true,
                    data: res
                })
            })


    }


    render = () => {

        const { loaded, data } = this.state;

        if( !loaded ){
            return(<h1> Loading...</h1>)
        }

        return(
            <ul>
                {
                    data.map( item => (
                        <label>                          
                        <h1>Post 
                            {item.id}</h1>                                    
                            <hr />
                            <hr />
                        <h1>Title: 
                            <hr>
                            </hr>
                            {item.title}</h1>                    
                        <h1>Body: 
                            <hr>
                            </hr>
                            {item.body}</h1>
                        <hr />
                        </label>  
                    ))
                }
            </ul>
        )
    }

}

export default List;