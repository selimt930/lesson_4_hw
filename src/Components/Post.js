import React, {Component} from 'react';

class Post extends Component{
    state = {
        loaded:false,
        data: null
    }

    componentDidMount() {
        const {match} = this.props;

        fetch(`https://jsonplaceholder.typicode.com/posts/${match.params.postid}`)
            .then( res => res.json() )
            .then( res => {
                console.log('ress', res );
                this.setState({
                    loaded: true,
                    data: res
                })
            })
    }

    render = () => {
        const { loaded, data } = this.state;
        if( !loaded ){
            return(<h1> Loading...</h1>);
        }

        return(
            <>
            {
            data.map( item =>(
            <>
                <h1> { item.title } </h1>
                <p>
                    { item.body}
                </p>
            </>
            ))
            }
            </>
        )

    }

}

export default Post