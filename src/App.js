import React from "react";
import List from "./Components/Posts";
import Post1 from "./Components/Post";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/List">List</Link>
            </li>
            <li>
              <Link to="/Post/">Post</Link>
            </li>
            <li>
              <Link to="/Comment">Comment</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/Post/:postid" component = {Post1}>
            < Post />
          </Route>
          <Route path="/Comment">
            <Comment />
          </Route>
          <Route path="/List">
            <List1 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function List1() {
  return <List/>;
}

const Comment = () => {
  return <></>;
}

function Post() {
  return <Post1/>;
}

/* import React from 'react';
import './App.css';
//import {Route, BrowserRouter} from 'react-router-dom';
//import Linker from './Components/Linker';
import List from './Components/Posts';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <List/>
      </header>
    </div>
  );
}

export default App; */